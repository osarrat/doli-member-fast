<!DOCTYPE html>
<html>
<head>
<title>Dolibarr: ajout rapide d'adhérent</title>
<meta charset="utf-8" />
<!-----Including CSS for different screen sizes----->
<link rel="stylesheet" type="text/css" href="css/responsiveform.css">
<link rel="stylesheet" media="screen and (max-width: 1200px) and (min-width: 601px)" href="css/responsiveform1.css" />
<link rel="stylesheet" media="screen and (max-width: 600px) and (min-width: 351px)" href="css/responsiveform2.css" />
<link rel="stylesheet" media="screen and (max-width: 350px)" href="css/responsiveform3.css" />
</head>
<body>
<div id="envelope">
