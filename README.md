# Dolibarr new FoundationMember fastform

Responsive form to easily add a new foundation member in Dolibarr through its API


### Requirements

**PHP requirements**
Requires to enable :
- allow_url_open

**Dolibarr requirements**
Has been tested only with Dolibarr 10.0.2

### Screenshots

**Home: 0. logging in**

![](screenshots/01_DoliMemberFast_Connexion.png)

**1: entering first name**

![](screenshots/11_DoliMemberFast_Prenom.png)

**2: listing matching foundation members**

![](screenshots/21_DoliMemberFast_ListeAdherents.png)

**3.1: updating yearly contribution of a selected member**

![](screenshots/31_DoliMemberFast_ActualiserAdhesion.png)

**3.2: creating a new member**

![](screenshots/32_DoliMemberFast_NouvelAdherent.png)
