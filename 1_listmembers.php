<?php
/**
 ***************************
 *    This file is part of DoliMemberFast .
 *
 *    DoliMemberFast is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    DoliMemberFast is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <gnu.org/licenses>.
 **************************/
require('inc/functions.php');
require('inc/header.php');

$membersList = api_members_by_firstname($_POST['firstname'], $_POST['token']);

?>

<header>
<h2>Dolibarr: ajout rapide d'adhérent</h2>
<p>2. liste d'adhérents existants  </p>
</header>


<strong>Y-a-t'il une fiche adhérent correspondante, et si oui la cotisation est-elle à jour ?</strong>

<?php
if(!is_null($membersList)) {
?>
<table border=1 cellspacing=1>
    <th>Prénom</th>
    <th>Nom</th>
    <th>Commune</th>
    <th>Fin cotisation</th>
    <th></th>
<?php
    foreach($membersList as $member) {
        $requiresNewSubscription = FALSE;
?>
    <tr>    
        <td><?=$member->firstname?></td>
        <td><?=$member->lastname?></td>
        <td><?=$member->town?></td>
        <td>
<?php
        $lastSubscriptionDateEnd = $member->last_subscription_date_end;
        if(is_null($lastSubscriptionDateEnd)) {
            $requiresNewSubscription = TRUE;
?>
            <span style="color:red">Aucune cotisation</span>
<?php
        } else {
            $lastSubscriptionDateEndString = date("d/m/Y", $lastSubscriptionDateEnd);
            if($lastSubscriptionDateEnd > time()) {
                echo '<span style="color:green">'.$lastSubscriptionDateEndString.'</span>';
            } else {
                $requiresNewSubscription = TRUE;
                echo '<span style="color:red">'.$lastSubscriptionDateEndString.'</span>';
            }
        }
?>
        </td>
        <td>
<?php
        if($requiresNewSubscription) {
?>
            <a href="2_createsubscription.php?id=<?=$member->id?>&token=<?=$_POST['token']?>">Enregistrer adhésion</a>
<?php

        }
?>
        </td>
    </tr>
<?php
    }
?>
</table>
<?php
} else {
?>
Aucune fiche adhérent trouvée avec ce prénom. <br />
<?php
}

?>

<p>
<strong>A. Oui, et l'adhérent est à jour :</strong><br />
<a href="javascript:history.back();">Revenir alors en arrière</a> 
</p>

<p>
<strong>B. Non, aucune fiche adhérent n'existe, et donc :</strong><br />
<a href="2_createsubscription.php?firstname=<?=$_POST['firstname']?>&token=<?=$_POST['token']?>">Créer une fiche adhérent pour <?=$_POST['firstname']?></a>

</p>

<?php require('inc/footer.php'); ?>

