<?php
/**
 ***************************
 *    This file is part of DoliMemberFast .
 *
 *    DoliMemberFast is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    DoliMemberFast is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <gnu.org/licenses>.
 **************************/

require('inc/header.php'); ?>
<form action="home.php" method="post">
<header>
<h2>Dolibarr: ajout rapide d'adhérent</h2>
<p>  </p>
</header>
<?php
if($_GET['error'] == "badloginpwd") {
?>
<div style="color:red;">Mauvais identifiant ou mot de passe.</div>
<?php
}
?>
<label>Identifiant</label>
<input name="login" placeholder="" type="text">
<label>Mot de passe</label>
<input name="password" placeholder="" type="password">
<input id="submit" type="submit" value="Envoyer">
</form>
<?php require('inc/footer.php'); ?>

