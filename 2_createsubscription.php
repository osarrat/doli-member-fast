<?php
/**
 ***************************
 *    This file is part of DoliMemberFast .
 *
 *    DoliMemberFast is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    DoliMemberFast is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <gnu.org/licenses>.
 **************************/
require('inc/functions.php');
require('inc/config.php');
require('inc/header.php');

$id = $_GET['id'];
$token = $_GET['token'];

$memberResponse = api_member_by_id($id, $token);

?>
<header>
<h2>Dolibarr: ajout rapide d'adhérent</h2>
<p>2. enregistrer une nouvelle adhésion  </p>
</header>


<form action="home.php" method="post">

<?php
if(!is_null($id)) {
    $member = $memberResponse;
?>

<input name="id" value="<?=$id?>" type="hidden">
<label>Id</label> : <?=$id?> <br />

<label>Prénom</label> : <?=$member->firstname?> <br />

<label>Nom</label> : <?=$member->lastname?> <br />

<label>Commune</label> : <?=$member->town?> <br />

<label>Email</label> : <?=$member->email?> <br />

<label>Téléphone</label> : <?=$member->phone_mobile?> <br />
<?php
} else {
?>
<!--<input name="id" value="" type="hidden"> -->

<label>Prénom*</label>
<input name="firstname" placeholder="" type="text" value="<?=$_GET['firstname']?>">

<label>Nom*</label>
<input name="lastname" placeholder="" type="text">

<label>Commune</label>
<input name="town" placeholder="" type="text">

<label>Email</label>
<input name="email" placeholder="" type="email"> <br />

<label>Téléphone</label>
<input name="phone_mobile" placeholder="" type="text">

<?php
}

?>

<p>
<strong>Cotisation</strong> <br />

<label>Date de cotisation</label> <br />
<input name="subscription_date" placeholder="" type="date" value="<?=date("Y-m-d")?>" > <i>Attention, date au format anglais mois-jour-année (ex pour Noël 2020: 12-25-2020)</i><br />

<label>Montant adhésion</label> <br />
<input name="amount" value="1" type="number"> € <br />

<label>Libellé adhésion</label> <br />
<input name="note" value="<?=$DEFAULT_NEW_SUBSCRIPTION_PARAMS['note']?>" type="text"> <br />

</p>

<input name="token" value="<?=$token?>" type="hidden">

<input id="submit" type="submit" value="Envoyer">
</form>

<?php require('inc/footer.php'); ?>

